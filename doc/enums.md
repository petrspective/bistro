# BISTRO ciselniky

### authorizedAccess

record type      | operation type        | idrecord
--- | --- | ---
1 - osoba           | 1 - čtení |
2 - skupina         | 2 - úprava |
3 - případ          | 3 - nový |
4 - hlášení         | 4 - přiložení souboru |
5 - novinky         | 5 - odstranění souboru |
6 - nástěnka        | 6 - provazba |
7 - symbol          | 7 - nová poznámka |
8 - uživatel        | 8 - smazání poznámky |
9 - body            | 9 - úprava poznámy |
10 - úkoly          | 10 - organizační zásah |
11 - audit          | 11 - smazání záznamu |
12 - jiné           | 12 - pokus o neoprávněný přístup |
13 - soubor         | 13 - pokus o přístup ke smazanému záznamu |
                    | 14 - vyhledávání|
                    | 15 - neopravněný pokus o řístup k tajnému |
                    | 16 - reset hesla |
                    | 17 - obnovení |
                    | 18 - uzamknutí |
                    | 19 - odemknutí |


### report type
type | typeName
--- | ---
1 | vyjezd
2 | vyslech

### p2ar role
role| roleName
--- | ---
0 | pritomen
1 | vyslychany
2 | vyslychajici
3 | zatceny
4 | velistel zasahu

## report status
status| statusName
--- | ---
0 | rozpracovane
1 | dokoncene
2 | analyzovane
3 | archivovane
