<?php

// GENERIC
$text['ulozit'] = 'Uložit';
$text['ano'] = 'ano';
$text['ne'] = 'ne';
$text['poslednizmena'] = 'Poslední změna';
$text['zmenil'] = 'Změnil';
$text['upravit'] = 'Upravit';
$text['smazat'] = 'smazat';
$text['obnovit'] = 'obnovit';
$text['nadpis'] = 'Nadpis';
$text['kategorie'] = 'Kategorie';
$text['obsah'] = 'Obsah';
$text['zavrit'] = 'Zavřít';
$text['notifikace'] = 'Notifikace';
$text['zapis'] = 'Zápis';
$text['cteni'] = 'Čtení';
$text['spravce'] = 'Správce';
$text['skryt'] = 'Skrýt';
$text['zpristupnit'] = 'Zpřístupnit';
$text['neznamo'] = 'Neznámo';
$text['nekdydavno'] = 'někdy dávno';

// LOGIN
$text['jmeno'] = 'Jméno';
$text['heslo'] = 'Heslo';
$text['prihlasit'] = 'Přihlásit';
$text['bistro'] = 'BIStro';
$text['odhlaseniuspesne'] = 'Byl jste úspěsně odhlášen.';

// CHYBOVKY
$text['zaznamnenalezen'] = 'Požadovaný záznam nebyl nalezen!';
$text['prazdnyvypis'] = 'Výpis neobsahuje žádné položky!';
$text['http401'] = 'You are unauthorized to make this request!';
$text['nevytvoreno'] = 'Chyba při vytváření, ujistěte se, že jste vše provedli správně a máte potřebná práva.';
$text['uzivatelexistuje'] = 'Uživatel již existuje, použijte jiné přihlašovací jméno!';
$text['vytvorenadmin'] = 'Vytvořen uživatel `admin` s heslem: ';
$text['soubornenalezen'] = 'Soubor nenalezen!';
$text['akcinelzeprovest'] = 'Požadovanou akci se nepodařilo uskutečnit!';
$text['nuceneodhlaseni'] = 'Z bezpečnostních důvodů jste byl odhlášen!';
$text['accessdeniedrecorded'] = 'Pokus o neoprávněný přístup zaznamenán!';


// MENU
$text['point'] = 'zlobod';
$text['menu-zlobody'] = 'Zlobody';
$text['hlaseniV'] = 'Hlášení';
$text['hlaseniM'] = 'hlášení';
$text['precistvse'] = 'Přečíst vše';
$text['opravduprecist'] = 'Opravdu označit vše jako přečtené?';
$text['hledat'] = 'Hledat';
$text['uzitecneodkazy'] = 'Odkazy';
$text['dalsi'] = 'Další';
$text['zalohovani'] = 'Zálohování';
$text['nastaveni'] = 'Nastavení';
$text['odhlasit'] = 'Odhlásit';
$text['audit'] = 'Audit';
$text['spravauzivatelu'] = 'Správa uživatelů';
$text['casovadostupnost'] = 'Časová dostupnost';
$text['ukoly'] = 'Úkoly';
$text['forum'] = 'Fórum/Analytika';
$text['skupiny'] = 'Skupiny';
$text['pripady'] = 'Případy';
$text['osoby'] = 'Osoby';
$text['aktuality'] = 'Aktuality';
$text['cesta'] = 'Cesta';
$text['akce'] = 'Akce';
$text['nastenka'] = 'Nástěnka';
$text['banka'] = 'BTB Banka';
$text['prazskahlidka'] = 'Pražská Hlídka';

// NASTAVENI
$text['uzivatel'] = 'Uživatel';
$text['timeout'] = 'Odhlásit po neaktivitě delší než';
$text['vevterinach'] = '30-1800 vteřin';
$text['puvodniheslo'] = 'Původní heslo';
$text['noveheslo'] = 'Nové heslo';
$text['noveheslokontrola'] = 'Nové heslo pro kontrolu';
$text['aktualniplan'] = 'Můj plán';
$text['heslanesouhlasi'] = 'Hesla nejsou stejná!';
$text['nastaveniulozeno'] = 'Nastavení uloženo.';
$text['timeoutnenicislo'] = 'Timeout není číslo, nastavení nebylo uloženo.';
$text['timeoutspatne'] = 'Timeout nesouhlasí, je buď příliš malý nebo příliš velký.';
$text['puvodniheslospatne'] = 'Nesouhlasí staré heslo, nastavení nebylo uloženo';
$text['email'] = 'E-mail';
$text['neplatnyemail'] = 'Neplatná e-mailová adresa.';

// UZIVATELE
$text['uzivatelskejmeno'] = 'Uživatelské jméno';
$text['upravituzivatele'] = 'Upravit uživatele';
$text['vytvorituzivatele'] = 'Vytvořit nového uživatele';
$text['cisloosoby'] = 'Hraná osoba';
$text['prava'] = 'Práva';
$text['uzivatelodstranen'] = 'Uživatelský účet odstraněn!';
$text['uzivatelobnoven'] = 'Uživatelský účet obnoven!';
$text['uzivatelzablokovan'] = 'Uživatelský účet zablokován!';
$text['uzivatelodblokovan'] = 'Uživatelský účet odblokován!';
$text['heslonastaveno'] = 'Nové heslo nastaveno: ';
$text['uzivatelvytvoren'] = 'Vytvořen uživatel: ';
$text['nemanedokoncenahlaseni'] = 'Uživatel nemá žádná nedokončená hlášení.';
$text['nedokoncenahlaseni'] = 'Rozpracovaná nedokončená hlášení';
$text['nemaneuzavrenepripady'] = 'Uživatel nemá žádný přiřazený neuzavřený případ.';
$text['nemanedokonceneukoly'] = 'Uživatel nemá žádné nedokončené úkoly.';
$text['vypisuzivatelu'] = 'Výpis uživatelů';
$text['naposledy'] = 'Naposledy';
$text['nikdy'] = 'Nikdy';
$text['odemknout'] = 'odemknout';
$text['zamknout'] = 'zamknout';
$text['opravdugenerovatheslo'] = 'Opravdu vygenerovat nové heslo pro uživatele';
$text['odemnkoutuzivatele'] = 'Odemknout uživatele';
$text['zamknoutuzivatele'] = 'Zamknout uživatele';
$text['smazatuzivatele'] = 'Smazat uživatele';
$text['obnovituzivatele'] = 'Obnovit uživatele';
$text['opravduodemnkout'] = 'Opravdu odemknout uživatele';
$text['opravduzamknout'] = 'Opravdu zamknout uživatele';
$text['opravdusmazat'] = 'Opravdu smazat uživatele';
$text['opravduobnovit'] = 'Opravdu obnovit uživatele';
$text['aclRoot'] = 'Administrátor';
$text['aclUser'] = 'Uživatelé';
$text['aclSecret'] = 'Tajné';
$text['aclAudit'] = 'Audit';
$text['aclGroup'] = 'Skupiny';
$text['aclPerson'] = 'Osoby';
$text['aclCase'] = 'Případy';
$text['aclHunt'] = 'Licence';
$text['aclReport'] = 'Reporty';
$text['aclSymbol'] = 'Symboly';
$text['aclGamemaster'] = 'Organizátor';
$text['aclAPI'] = 'API';
$text['aclNews'] = 'Aktuality';
$text['aclBoard'] = 'Nástěnka';


// BACKUP
$text['existujicizalohy'] = 'Existující zálohy';
$text['zalohovatrucne'] = 'Vytvořit zálohu ručně';
$text['zalohavytvorena'] = 'Záloha vytvořena!';
$text['stahnoutzalohu'] = 'Stáhnout zálohu';
$text['zalohynenalezeny'] = 'Zálohy nenalezeny!';

// NASTENKA / AKTUALITY
$text['obsahnastenky'] = 'Obsah nástěnky';
$text['upravitnastenku'] = 'Upravit nástěnku';
$text['osobninastenka'] = 'Osobní nástěnka';
$text['verejnanastenka'] = 'Veřejná nástěnka';
$text['zobrazitnastenku'] = 'Zobrazit nástěnku';
$text['zadnanedokoncenahlaseni'] = 'Nemáte žádná nedokončená hlášení';
$text['zadnenadokoncenepripady'] = 'Nemáte žádný přiřazený neuzavřený případ';
$text['zadnenedokonceneukoly'] = 'Nemáte žádné nedokončené úkoly';
$text['nanedokoncenahlaseni'] = 'Rozpracovaná nedokončená hlášení';
$text['neuzavrenepripady'] = 'Přiřazené neuzavřené případy';
$text['nedokonceneukoly'] = 'Nedokončené úkoly';
$text['nastenkaprazdna'] = 'Veřejná nástěnka nemá žádný obsah';
$text['nastenkaupravena'] = 'Nástěnka upravena';
$text['zobrazitaktuality'] = 'Zobrazit aktuality';
$text['pridataktualitu'] = 'přidat aktualitu';
$text['aktualitaodebrana'] = 'Aktualita odebrána';
$text['aktualitaneodebrana'] = 'Aktualitu se nepodařilo odebrat';
$text['aktualitavlozena'] = 'Aktualita vložena';
$text['aktualitanevlozena'] = 'Aktualitu se nepodařilo vložit';
$text['novaaktualita'] = 'Nová aktualita';
$text['kategorieherni'] = 'herní';
$text['kategoriesystemova'] = 'systémová';
$text['aktualitaobnovena'] = 'Aktualita obnovena';
$text['aktualitaneobnovena'] = 'Aktualitu se nepodařilo obnovit';

// FILTERS
$text['jennove'] = 'Jen nepřečtené';
$text['iuzavrene'] = 'I uzavřené';
$text['imrtve'] = 'I mrtvé';
$text['iarchiv'] = 'I archiv';
$text['itajne'] = 'I tajné';
$text['archivovano'] = 'archivováno';
$text['utajeno'] = 'utajeno';
$text['smazany'] = 'smazáný';
$text['smazane'] = 'Smazané';
$text['portrety'] = 'Portréty';
$text['poznamky'] = 'Poznámky';
$text['symboly'] = 'Symboly';
$text['strana'] = 'Strana';
$text['specializace'] = 'Specializace';
$text['vse'] = 'vše';
$text['neznama'] = 'neznámá';
$text['prvni'] = 'první';
$text['druha'] = 'druhá';
$text['treti'] = 'třetí';
$text['ctvrta'] = 'čtvrtá';
$text['pata'] = 'pátá';
$text['sesta'] = 'šestá';
$text['sedma'] = 'sedmá';
$text['mimokategorie'] = 'mimo kategorie';
$text['svetlo'] = 'světlo';
$text['tma'] = 'tma';
$text['clovek'] = 'člověk';
$text['bilymag'] = 'bilý mág';
$text['cernymag'] = 'černý mág';
$text['lecitel'] = 'léčitel';
$text['obraten'] = 'obrateň';
$text['upir'] = 'upír';
$text['vlkodlak'] = 'vlkodlak';
$text['vedma'] = 'vědma';
$text['zarikavac'] = 'zaříkávač';
$text['vykladac'] = 'vykladač';
$text['jasnovidec'] = 'jasnovidec';


// CASES
$text['pridatpripad'] = 'Přidat případ';
$text['nazev'] = 'Název';
$text['stav'] = 'Stav';
$text['zmeneno'] = 'Změněno';
$text['uzavreny'] = 'uzavřený';
$text['otevreny'] = 'otevřený';
$text['pridatpoznamku'] = '';
$text['opravdusmazatpripad'] = 'Opravdu smazat případ';
$text['opravduobnovitpripad'] = 'Opravdu obnovit případ';
$text['vytvoreno'] = 'Vytvořeno';
$text['editpripad'] = 'Upravit případ';
$text['smazatpripad'] = 'Smazat případ';
$text['obnovitpripad'] = 'Obnovit případ';

//GROUPS
$text['pridatskupinu'] = 'Přidat skupinu';
$text['upravitskupinu'] = 'Upravit skupinu';
$text['smazatskupinu'] = 'Smazat skupinu';
$text['obnovitskupinu'] = 'Obnovit skupinu';
$text['opravdusmazatskupinu'] = 'Opravdu smazat skupinu?';
$text['opravduobnovitskupinu'] = 'Opravdu obnovit skupinu?';
$text['aktivni'] = 'aktivní';

//REPORTS
$text['vyslychany'] = 'vyslíchaný';
$text['vyslychajici'] = 'vyslíchající';
$text['velitel'] = 'velitel zásahu';
$text['zatceny'] = 'zatčený';
$text['pritomny'] = 'přítomný';

//PERSONS
$text['mrtvy'] = 'mrtvý';
$text['upravitosobu'] = 'Upravit osobu';
$text['smazatosobu'] = 'Smazat osobu';
$text['obnovitosobu'] = 'Obnovit osobu';
$text['opravdusmazatosobu'] = 'Opravdu smazat osobu?';
$text['opravduobnovitosobu'] = 'Opravdu obnovit osobu?';
$text['pridatosobu'] = 'přidat osobu';

//SYMBOLS
$text['neprirazenesymboly'] = 'nepřiřazené symboly';
$text['vyhledatsymbol'] = 'vyhledat symbol';

//PERSONS
$text['telefon'] = 'Telefon';
$text['definitions'] = 'Zařazení';
$text['kontakty'] = 'Kontakty';
$text['kategorie'] = 'Kategorie';
$text['strana'] = 'Strana';
$text['specializace'] = 'Specializace';
